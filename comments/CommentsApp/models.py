from django.db import models


class CommentData(models.Model):
    author = models.CharField(max_length=200)
    text = models.CharField(max_length=500,blank=True,null=True)
    date = models.DateTimeField(auto_now_add=True,blank=True,null=True)
    
    class Meta:
        verbose_name ="Comment"
        verbose_name ="Comments"
    
    def __unicode__(self):
        return self.author

    


class CommentTree(models.Model):
    comment_id = models.OneToOneField(CommentData)
    parent_id = models.ForeignKey('self',null=True,blank=True)
    votes = models.IntegerField(default=0)
    level = models.IntegerField(default=0,blank=True,null=True)
    queue = models.IntegerField(default=0,blank=True,null=True)
    
    
    class Meta:
        verbose_name ="Comment tree"
    
    def __unicode__(self):
        return self.pk

    
#    
#class AnotherComments(models.Model):
#    parent_id = models.ForeignKey('self',null=True,blank=True)
#    votes = models.IntegerField(default=0)
#    level = models.IntegerField(default=0,blank=True,null=True)
#    queue = models.IntegerField(default=0,blank=True,null=True)
#    author = models.CharField(max_length=200)
#    text = models.CharField(max_length=500,blank=True,null=True)
#    date = models.DateTimeField(auto_now_add=True,blank=True,null=True)